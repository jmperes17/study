<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Shopping Cart </title>
<meta charset="utf-8">

<!-- Include bootstrap library -->
<link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">

<!-- Include custom css -->
<link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">

</head>
<body>
<div class="container">
	<h2>PRODUCTS</h2>
	
	<!-- Cart basket -->
	<div class="cart-view">
		<a href="<?php echo base_url('cart'); ?>" title="View Cart"><i class="icart"></i> (<?php echo ($this->cart_lib->total_items() > 0)?$this->cart_lib->total_items().' Items':'Empty'; ?>)</a>
	</div>

	<?php 
	
	#$this->load->model('Product');
	
	#sql = "SELECT * FROM `products`";
	# ($this->db->query($sql))
	#{
   	 	#$sql = 'SELECT * FROM products';    
    	#$query = $this->db->query($sql);
    	#$query->result_array(); 
    
		#echo 'Total Results: ' . $query->num_rows();
	#}

   #?>

	
	<!-- List all products -->
    <div class="row col-lg-12">
	<?php if(!empty($products)){ foreach($products as $row){ ?>
			<div class="card col-lg-3">
				<img class="card-img-top" src="<?php echo base_url('uploads/product_images/'.$row['image']); ?>" alt="">
				<div class="card-body">
				  <h5 class="card-title"><?php echo $row["name"]; ?></h5>
				  <h6 class="card-subtitle mb-2 text-muted">Price: <?php echo '$'.$row["price"].' USD'; ?></h6>
				  <p class="card-text"><?php echo $row["description"]; ?></p>
				  <a href="<?php echo base_url('Products/addToCart/'.$row['id']); ?>" class="btn btn-primary">Add to Cart</a>
				</div>
			</div>
		<?php } }else{ ?>
			<p>Product(s) not found...</p>
		<?php } ?>
    </div>
</div>
</body>
</html>