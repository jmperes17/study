<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller{
	
	function  __construct(){
		parent::__construct();
		
		// Load cart library
		$this->load->library('cart_lib');
		
		// Load product model
		$this->load->model('M_product');

		$this->load->helper('url');
	}
	
	function index(){
		$data = array();
		
		// Fetch products from the database
        $data['products'] = $this->M_product->getRows();
		
		// Load the product list view
		$this->load->view('products/index', $data);
	}
	
	function addToCart($proID){
		
		// Fetch specific product by ID
        $product = $this->M_product->getRows($proID);
		
		
		// Add product to the cart
		$dataProduto = array(
			'id'	=> $product['id'],
			'qty'	=> 1,
			'price'	=> $product['price'],
			'name'	=> $product['name'],
			'image' => $product['image']
		);

		$this->cart_lib->insert($dataProduto);
		
		#var_dump($data);
		
		
		
		// Redirect to the cart page
		#redirect('Cart/listCart', $dataProduto);
		$this->load->view('cart/index', $dataProduto);
		
	}
	
}