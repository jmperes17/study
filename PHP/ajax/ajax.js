
function buscaDados() {
    var nome = document.getElementById("buscanome").value;
    var result = document.getElementById("dados");

    result.innerHTML = '<img src="loading.gif" width="200px">';

    var ajax = new XMLHttpRequest();

    ajax.open("GET", "processa.php?buscanome=" + nome, true);

    ajax.onreadystatechange = function () {
        if (ajax.readyState == 4) {
            if (ajax.status == 200) {
                result.innerHTML = ajax.responseText;
            }
            else {
                result.innerHTML = "ERRO!" + ajax.statusText;
            }
        }
    };

    ajax.send(null);
}



function insereDados() {
    var nome = document.getElementById("insereNome").value;
    var email = document.getElementById("insereEmail").value;
    var telefone = document.getElementById("insereTelefone").value;
    var endereco = document.getElementById("insereEndereco").value;

    var result = document.getElementById("resposta");

    var ajax = new XMLHttpRequest();
    result.innerHTML = '<img src="loading.gif" width="200px">';
    ajax.open("GET", "processa.php?nome=" + nome + "&email=" + email + "&telefone=" + telefone + "&endereco=" +
        endereco, true);

    ajax.onreadystatechange = function () {
        if (ajax.readyState == 4) {
            if (ajax.status == 200) {
                result.innerHTML = ajax.responseText;
            }
            else {
                result.innerHTML = "ERRO!" + ajax.statusText;
            }
        }
    };

    ajax.send(null);


}

function deletaUsuario(id){

    var result = document.getElementById("dados");

    var ajax = new XMLHttpRequest();

    result.innerHTML = '<img src="loading.gif" width="200px">';


    ajax.open("GET", "processa.php?deleta="+id, true);

    ajax.onreadystatechange = function(){
        if(ajax.readyState == 4){
            if(ajax.status == 200){
                result.innerHTML = ajax.responseText;
            }
            else{
                result.innerHTML = "ERRO!" + ajax.statusText;
            }
        }
    };

    ajax.send(null);
}

