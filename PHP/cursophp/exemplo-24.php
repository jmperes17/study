<?php

interface Pessoa
{
    public function falar($texto);
    public function andar($velocidade);
    public function correr($velocidade);
}

class crianca implements Pessoa
{

    public function falar($frase)
    {

        echo $frase;
    }

    public function andar($velocidade)
    {
        echo $velocidade;
    }

    public function correr($velocidade)
    {
        echo $velocidade;
    }
}

$junior = new crianca;

$junior->falar("I saw you said");

echo "<br>";

$junior->andar("1km/h");
echo "<br>";
$junior->correr("20km/h");


var_dump($junior);
