<?php

class Pessoa {
    public $nome;

    public function falar(){
        return " Meu nome é: ". $this->nome;
    }
}

$junior = new pessoa;

$junior->nome = "Junior";
echo $junior->falar();
echo "<br>";

class Carro {

    private $modelo;
    private $motor;
 

    public function getModelo(){
        return $this->modelo;
    }

    public function setModelo($modelo){
        return $this->modelo = $modelo;
    }

    public function getMotor(){

        return $this->motor;
    }

    public function setMotor($motor){
        return $this->motor = $motor;
    }

    public function Exibir(){
        return array(
            "modelo" =>$this->getModelo(),
            "motor" =>$this->getMotor()
        );
    }

    


}
$fusca = new Carro;
$fusca->setModelo("Fusca 2001");
$fusca->setMotor("203cc");
echo $fusca->exibir();

