<?php


abstract class Crianca
{

    public function falar($frase = "blabla")
    {

        echo $frase;
    }

    public function andar($velocidade = "5km/h")
    {
        echo $velocidade;
    }

    public function correr($velocidade = "5km/h")
    {
        echo $velocidade;
    }
}

class Baby extends Crianca
{
    public function falar($frase = "bilubilu")
    {
        echo $frase;
    }
}

class Kid extends Crianca
{
    public function falar($frase = "bilubilu")
    {
        // Usa o metodo correr da classe Pai
        echo $frase." e ".parent::correr("10km/h");
    }
}

$junior = new Baby;

$junior->falar("I feel  good, because you put your butt on me");
echo "<br/>";

$junior->correr();

echo "-----------------------<br>";

$jao = new Kid;

$jao->falar("I feel  good, because you put your butt on me");
echo "<br/>";

$jao->correr();

