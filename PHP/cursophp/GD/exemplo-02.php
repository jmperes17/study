<?php
date_default_timezone_set('America/Sao_Paulo');
$image = imagecreatefromjpeg("modelo_boleto_img.jpg");

$titleColor = imagecolordeallocate($image, 0, 0, 0);//Preto
$gray = imagecolordeallocate($image, 100, 100, 100);//Cinza

imagettftext($image, 32, 0, 750, 950, $titleColor, "fonts".DIRECTORY_SEPARATOR."Bevan"
.DIRECTORY_SEPARATOR."Bevan-Regular.ttf", "BOLETO JM SPORTS");//Imagem, tamanho, disposição, altura, string, cor
imagettftext($image, 32,0, 700, 1050, $titleColor, "fonts".DIRECTORY_SEPARATOR."Bevan"
.DIRECTORY_SEPARATOR."Bevan-Regular.ttf","Nome: Fulano de Tal da Silva Sauro");
imagettftext($image, 32,0, 700, 1070, $titleColor,"fonts".DIRECTORY_SEPARATOR."Bevan"
.DIRECTORY_SEPARATOR."Bevan-Regular.ttf", "VALOR: R$ 120");
imagejpeg($image, 32,0, 700, 1090, "Vencimento: ". date("d-m-Y", strtotime("+5 day")), $titleColor);
imagejpeg($image, 32,0, 700, 1500, "Gerado em: ". date("d-m-Y H:i:s"), $titleColor);



header("Content-type: image/jpeg");//Tipo 


imagejpeg($image, "boleto-".date("d-m-Y").".jpg");// nome dinâmico

imagedestroy($image);



?>