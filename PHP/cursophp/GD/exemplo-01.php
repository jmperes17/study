<?php
date_default_timezone_set('America/Sao_Paulo');
$image = imagecreatefromjpeg("modelo_boleto_img.jpg");

$titleColor = imagecolordeallocate($image, 0, 0, 0);//Preto
$gray = imagecolordeallocate($image, 100, 100, 100);//Cinza

imagestring($image, 5, 750, 950, "BOLETO JMSPORTS", $titleColor);//Imagem, tamanho, disposição, altura, string, cor
imagestring($image, 5, 700, 1050, "Nome: Fulano de Tal da Silva Sauro", $titleColor);
imagestring($image, 5, 700, 1070, "VALOR: R$ 120", $titleColor);
imagestring($image, 5, 700, 1090, "Vencimento: ". date("d-m-Y", strtotime("+5 day")), $titleColor);
imagestring($image, 5, 700, 1500, "Gerado em: ". date("d-m-Y H:i:s"), $titleColor);



header("Content-type: image/jpeg");//Tipo 


imagejpeg($image, "boleto-".date("d-m-Y").".jpg");// nome dinâmico

imagedestroy($image);



?>