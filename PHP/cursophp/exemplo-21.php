<?php

class Endereco {
    private $logradouro;
    private $numero;
    private $complemento;

    public function __construct($a, $b, $c){
        $this->logradouro = $a;
        $this->numero = $b;
        $this->complemento = $c;

    }    

    public function __toString()
    {
        return $this->logradouro.", ".$this->numero.", ".$this->complemento;
    }
    
}

$cidade = new Endereco("Travessa francisco", "123", "Casa do Junior");

echo $cidade;



