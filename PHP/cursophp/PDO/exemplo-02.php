<?php

//Conecao utilizando PDO
$conn = new PDO("mysql:dbname=crud; host=localhost","root","");

//Instanciando statement com a query
$stmt = $conn->prepare("INSERT INTO contatos (nome, sobrenome) VALUES(:NOME, :SOBRENOME)");

$nome = "Jozé";
$sobrenome = "Junior";

//Parâmetros da query
$stmt->bindParam(":NOME", $nome);
$stmt->bindParam(":SOBRENOME", $sobrenome);

//Executando a query
if($stmt->execute()){
    echo "Inserido";
}
