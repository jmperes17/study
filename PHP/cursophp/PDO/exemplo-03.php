<?php

//Conecao utilizando PDO
$conn = new PDO("mysql:dbname=crud; host=localhost","root","");

//Instanciando statement com a query
$stmt = $conn->prepare("UPDATE contatos SET nome=:NOME, sobrenome=:SOBRENOME WHERE id_contato=:ID");

$id = 32;
$nome = "Jozé Martins";
$sobrenome = " Peres Junior";

//Parâmetros da query
$stmt->bindParam(":NOME", $nome);
$stmt->bindParam(":SOBRENOME", $sobrenome);
$stmt->bindParam(":ID", $id);

//Executando a query
if($stmt->execute()){
    echo "Alterado";
}
