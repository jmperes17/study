<?php

//Conexao utilizando PDO
$conn = new PDO("mysql:dbname=crud; host=localhost", "root", "");

$conn->beginTransaction();

//Instanciando statement com a query
$stmt = $conn->prepare("DELETE FROM contatos WHERE id_contato = ?");

$id = 32;

//Executando a query
$stmt->execute(array($id));


//Cancelando a transação
#$conn->rollBack();

//Confirmando a transação
$conn->commit();
echo "EXCLUIDO COM SUCESSO";