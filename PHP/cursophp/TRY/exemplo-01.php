<?php


function trataNome($nome)
{
    if (!$nome) {
        throw new Exception("Nenhum nome informado!", 1);
    }
    echo ucfirst($nome . "<br>");
}

try {
    trataNome("Jozé") . "<br>";
    trataNome("") . "<br>";
} catch (exception $e) {

    echo json_encode(array(
        "message" => $e->getMessage(),
        "file" => $e->getFile(),
        "line" => $e->getLine(),
        "code" => $e->getCode()
    ));
}
