<?php

use crianca as GlobalCrianca;

interface Pessoa
{
    public function falar($texto);
    public function andar($velocidade);
    public function correr($velocidade);
}

abstract class Crianca implements Pessoa
{

    public function falar($frase)
    {

        echo $frase;
    }

    public function andar($velocidade)
    {
        echo $velocidade;
    }

    public function correr($velocidade)
    {
        echo $velocidade;
    }
}

class Baby extends Crianca{

}

$junior = new Baby;

$junior->falar("I feel  good, because you put your butt on me");
