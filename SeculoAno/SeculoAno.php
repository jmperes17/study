<?php


$ano = $_POST['ano'];

if ($ano <= 0 || strlen($ano) != 4) {        
    echo  "<script>alert('Valor inválido! Por gentileza, informar numero maior que 0 e no formato AAAA');</script>";
    echo "<script>window.location='index.php';</script>";
}
 else {
    function SeculoAno($ano)
    {
        $seculo = 0;

        if (intval(substr($ano, -2)) == 00) {

            $seculo =  intval(substr($ano, 0, 2));
        } else {
            $seculo = intval(substr($ano, 0, 2)) + 1;
        }
        return $seculo;
    }

    echo '<p> O século correspondente ao ano ' . $ano . ' é: ' . SeculoAno($ano);
}
