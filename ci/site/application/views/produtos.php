<?php $this->load->view('menu'); 

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- bootstrap core css -->
  <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap-pro.css" />
  <!-- font awesome style -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
  <link rel="stylesheet" href="../assets/css/fontawesome.min.css">
  <!-- Custom styles for this template -->
  <link href="../assets/css/style-pro.css" rel="stylesheet" />
  <!-- responsive style -->
  <link href="../assets/css/responsive.css" rel="stylesheet" />
</head>


<body class="sub_page">
  <section class="product_section layout_padding">
    <div class="container">
      <div class="heading_container heading_center">
        <h2>
          Nossos <span>Produtos</span>
        </h2>
      </div>
      <div class="row">
        <?php foreach ($produtos as $row) { ?>
          <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="box">
              <div class="option_container">
                <div class="options">
                  <!--<a href="<?php echo base_url('camisa_1'); ?>" class="option1">
                    Details
                  </a>-->
                  <a href="<?php echo base_url('Produtos/detalhes_produto')?>" class="option2">
                    Adicionar ao carrinho
                  </a>
                </div>
              </div>
              <div class="img-box">
                <img src="<?php echo base_url('../assets/uploads/product_images/' . $row['imagem']); ?>" alt="">
              </div>
              <div class="detail-box">
                <h5>
                  <?= $row['nome']; ?>
                </h5>

                <h6>
                  <?= $row['preco'] . ' R$'; ?>
                </h6>
              </div>
            </div>
          </div><!-- col-sm-6 col-md-4 col-lg-3 -->
        <?php } ?>
      </div><!-- row -->

      <?php
      if (isset($_GET['addToCart'])) {
        //Adicionando ao carrinho
        $id = (int)$_GET['addToCart'];
        if (isset($produtos[$id])) {
          if (isset($_SESSION['carrinho'][$id])) {
            $_SESSION['carrinho'][$id]['qty']++;
          } else {
           $_SESSION['carrinho'][$id] = array(
              'qty' => 1, 'nome' => $produtos[$id]['nome'],
              'preco' => $produtos[$id]['preco']
            );
          }
          echo "<script>alert('Produto adicionado ao carrinho')</script>";
        } else {
            die("Não existe");
        }
      }

      ?>
    </div>
  </section>

  <div>
    <h2 class="title"> Carrinho: </h2>

    <?php
        foreach($_SESSION['carrinho'] as $key => $value){
          //Nome do produto
          //Quantidade
          //Preço
          echo '<p>Nome do produto : '.$value['nome'].' | Quantidade: '.$value['qty'].' | Preço: '.($value['qty']*$value['preco']).'</p>';
          echo '<hr>';
        }
    ?>    
  </div>

</body>
<?php
$this->load->view('footer');
?>

</html>