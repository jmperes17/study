<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>

  html {height:100%;}


    .container_login{
      text-align: center; 
    }
    .check{
      text-align: left;
    }
    .inputs{
	    width: 70%;
	    margin: 0 auto;
	    padding: 20px;
	    border: black;
      background: transparent;
	    margin-bottom: 10px;
	    border-radius: 50px;
      outline:solid silver 0.5px;

      

      
    }


    .btn{
	    padding: 10px;
      
	    width: 70%;
	    margin: 0 auto;
	    border-radius: 30px;
	    border: none;
	    text-transform: uppercase;
	    font-weight: 500;
	    color: #0031B2;
	    background: linear-gradient(to right, blue, blue);
	    cursor: pointer;
    }

    body{
	  background-image:  url("background.jpeg");
    background-repeat: no-repeat;
    background-size: 100% 100%;
    background-position: center center;
    margin-right: 200px;
    background-attachment: fixed;

    min-height:100%;
    position:center center;
    width:cover;

  
   
    }

    .menu{
      position:absolute;
	    left:50%;
	    top:50%;
	    transform: translate(-50%,-50%);
      background-color: white;
	    border-radius:10px;
	    padding:10px 80px;
    }

    

    .footer {
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    background-color: transparent;
    color: silver;
    text-align: center;
}

    </style>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

    <title>Login</title>
  </head>
  <body >

    <div class="col-md-12 mt-5">
                
            
            <div class="menu">
            <div class="container container_login" id="login_container">
              <h3 class="text-center" style="color: blue;background-color: none;"><input type="image" width="160" height="50" id="image"  alt="Login" src="file2.jpg"></h3>

                <form action="../Controller/valida_usuario.php" method="post">
                  
                    <h4>Bem vindo ao Command Center</h4>
                    <p>Mais controle sobre sua gestão</p>
                    
                    <input type="email" name="email" placeholder="email" id="email" class="inputs" value="email@email.com"><br>
                    <input type="password" name="senha" placeholder="senha" id="senha" class="inputs" value="senha"><br>
                    <input type="checkbox" name="checar" class="check"  id="check"> Mantenha conectado<br><br>

                    
                    
                    <button type="submit" name="submit" class="btn btn-primary">Entrar</button><br><br>

                    <a href="cadastro.html" style="color: #0031B2; text-decoration:none;"> Esqueceu sua senha?</a><br>
                    

                    

                    

                </form>
                
            </div> 
          
    </div>

              
    <div>

        <footer class="footer">
          <p>@2021 Conexa Saúde. Todos os direitos reservados</p>
        </footer>

    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

    
  </body>
  
  
</html>